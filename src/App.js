import React from "react";
import { connect } from "react-redux";
import Slider from "./component/Slider";
import "./App.css";

function App(props) {
  const { isFetching, photos, error } = props;
  //useEffect(()=>fetchPhotosSaga(),[])
  return (
    <div className="App">
      {!isFetching && (
        <Slider loading={isFetching} data={photos} error={error} />
      )}
    </div>
  );
}

function mapStateToProps(state) {
  return {
    isFetching: state.gallery.isFetching,
    photos: state.gallery.photos,
    error: state.gallery.error
  };
}
const fetchPhotosSaga = () => dispatch => {
  dispatch({ type: "FETCH_PHOTOS_SAGA" });
};
export default connect(
  mapStateToProps,
  { fetchPhotosSaga }
)(App);
