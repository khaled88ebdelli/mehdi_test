import 'react-responsive-carousel/lib/styles/carousel.min.css';
import './style.css'
import React, {Component} from 'react';
import {Carousel} from 'react-responsive-carousel';



export default class CarouselComponent extends Component {
  state = { deviceType: ''};

  componentDidMount() {
    this.setSize();
  }

  setSize = () => {
    if (window.innerWidth <= 1024 && window.innerWidth > 464) {
      this.setState({deviceType: 'tablet'});
    } else if (window.innerWidth > 1024) {
      this.setState({deviceType: 'desktop'});
    } else {
      this.setState({deviceType: 'mobile'});
    }
  };

  render() {

    return (

   <div>
    {this.state.deviceType==='desktop'&&(
    <Carousel
        centerMode
        centerSlidePercentage={50}  
        autoPlay interval={2000} 
        emulateTouch
     >
        {this.props.data.map((elm,index) =>
         <div key={index}>
         <img alt='gallery' src={elm.url}/>
         </div>
        )}
     </Carousel>)}

    {this.state.deviceType==='mobile'&&(
    <div>
    {this.props.data.map((elm,index) =>
      <div key={index}>
      <img   alt='gallery' src={elm.url} />
      </div>
     )}
    </div>
    )}

    {this.state.deviceType === 'tablet' &&(
      <div>
      {this.props.data.map((elm, index) => 
      <span key ={index}>
      <img alt ='gallery' src ={elm.url} />
      </span>
     )}
      </div>
    )}
    </div>
    )}}
