import {
    FETCH_PHOTOS_ERROR,
    FETCH_PHOTOS_START,
    FETCH_PHOTOS_SUCCESS
  } from "./actionTypes";
  
  export const fetchPhotosError = (payload) => {
    return { type: FETCH_PHOTOS_ERROR, payload };
  };
  
  export const fetchPhotosStarted = () => {
    return { type: FETCH_PHOTOS_START };
  };
  
  export const fetchPhotosSuccess = payload => {
    return { type: FETCH_PHOTOS_SUCCESS, payload };
  };
  