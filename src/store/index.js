import { createStore, applyMiddleware } from "redux";
import rootReducer from "./reducers";
import { createLogger } from "redux-logger";
import createSagaMiddleware from "redux-saga";
import watchFetchPhotosSaga from "./sagas/fetchPhotos";
import { all,put,fork} from 'redux-saga/effects';
import { fetchPhotosStarted } from "./actions/index";

//create logger Middleware
const loggerMiddleware = createLogger();
//init
const sagaMiddleware = createSagaMiddleware();
//middlewares
const middlewares = [sagaMiddleware, loggerMiddleware];
//create store
const store = createStore(rootReducer, applyMiddleware(...middlewares));
function* initSaga() {
    yield put(fetchPhotosStarted());
  }
function* rootSaga() {
    yield fork(initSaga)
    yield all([
        watchFetchPhotosSaga()
    ]);
  }
sagaMiddleware.run(rootSaga);

export default store;
