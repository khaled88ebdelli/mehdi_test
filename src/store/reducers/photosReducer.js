import {
    FETCH_PHOTOS_START,
    FETCH_PHOTOS_SUCCESS,
    FETCH_PHOTOS_ERROR
  } from "../actions/actionTypes";
  
  const initialState = {
    photos: [],
    isFetching: false,
    error: null
  };
  const photosReducer = (state = initialState, action) => {
    switch (action.type) {
      case FETCH_PHOTOS_START:
        return { ...state, isFetching: true };
      case FETCH_PHOTOS_SUCCESS:
        return {
          ...state,
          photos: action.payload.data,
          isFetching: false
        };
      case FETCH_PHOTOS_ERROR:
        return { ...state, isFetching: false, error: action.payload };
      default:
        return state;
    }
  };
  
  export default photosReducer;
  