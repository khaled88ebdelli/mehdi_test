import { takeLatest, put } from "redux-saga/effects";
import API from "../../config/Api";
import {
  
  FETCH_PHOTOS_START,
  FETCH_PHOTOS_SUCCESS,
  FETCH_PHOTOS_ERROR
} from "../actions/actionTypes";

export default function* watchFetchPhotosSaga() {
  yield takeLatest(FETCH_PHOTOS_START, fetchPhotosSaga);
}

function* fetchPhotosSaga() {
  try {
    const url='photos?_start=0&_limit=40'
    const photosResponse = yield API.get(url);
    const { data } = yield photosResponse;

    yield put({ type: FETCH_PHOTOS_SUCCESS, payload: { data } });
  } catch (error) {
    yield put({ type: FETCH_PHOTOS_ERROR, error });
  }
}
